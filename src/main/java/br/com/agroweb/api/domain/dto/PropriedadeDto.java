package br.com.agroweb.api.domain.dto;

import java.time.LocalDate;

import br.com.agroweb.api.domain.Estado;
import lombok.Getter;
import lombok.Setter;
 
@Getter
@Setter
public class PropriedadeDto {
	private Long id;
	private String cidade;
	private Estado estado;
	private String bairro;
	private String nome;
	private String cpfCnpj;
	private LocalDate dataConstituicao;
	private String admResp;
	private String celular;
	private String telefone;
	private String email;
	private String logradouro;
	private String numero;
	private String cep;
	private String complemento;
	private String descricao;
}
