package br.com.agroweb.api.domain.dto;

import java.time.LocalDate;
import java.util.List;

import br.com.agroweb.api.domain.Pessoa;
import br.com.agroweb.api.domain.Proponente;
import br.com.agroweb.api.domain.Propriedade;
import br.com.agroweb.api.domain.ServicosProposta;
import lombok.Data;

@Data
public class ContratoPropostaDto {
	
	private Long id;
	private Propriedade propriedade;
	private Proponente proponente;
	private Pessoa pessoa;
	private String apresentacao;
	private String objetivo;
	private String cronograma;
	private String formaPagamento;
	private String condicoesNecessarias;
	private String localizacaoExecucao;
	private String obs;
	private LocalDate dataInicio;
	private LocalDate dataFim;
	private String prazoValidade;
	private LocalDate dataAtualizacao;
	private List<ServicosProposta> servicos;
}
