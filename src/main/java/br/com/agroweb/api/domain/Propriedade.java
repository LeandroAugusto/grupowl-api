package br.com.agroweb.api.domain;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "TB_PROPRIEDADE")
public class Propriedade {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "ID")
	private Long id;
	
	@Column(name = "CPFCNPJ")
	private String cpfCnpj;
	
	@Column(name = "NOME")
	private String nome;
	
	@Column(name = "ADMINISTRADORESPONSAVEL")
	private String admResp;
	
	@Column(name = "DATACONSTITUICAO")
	private LocalDate dataConstituicao;
	
	@Column(name = "CELULAR")
	private String celular;

	@Column(name = "TELEFONE")
	private String telefone;

	@Column(name = "EMAIL")
	private String email;

	@Column(name = "LOGRADOURO")
	private String logradouro;

	@Column(name = "NUMERO")
	private String numero;

	@Column(name = "COMPLEMENTO")
	private String complemento;
	
	@ManyToOne
	@JoinColumn(name = "ID_ESTADO", referencedColumnName = "ID")
	private Estado estado;
	
	@Column(name = "CIDADE")
	private String cidade;

	@Column(name = "BAIRRO")
	private String bairro;

	@Column(name = "CEP")
	private String cep;

	@Column(name = "DESCRICAO")
	private String descricao;

	@Column(name = "SITUACAO")
	private Integer situacao;

	public void situacaoAtiva() {
		this.situacao = 1;
	}

	public void situacaoInativa() {
		this.situacao = 0;
	}
	
	@PreUpdate
	public void preUpdate() {
		this.situacaoAtiva();
	}
	
	@PrePersist
	public void prePersist() {
		this.situacaoAtiva();
	}
}
