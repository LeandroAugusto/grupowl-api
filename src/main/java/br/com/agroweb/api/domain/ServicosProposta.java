package br.com.agroweb.api.domain;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "TB_SERVICOS_PROPOSTA")
public class ServicosProposta {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "ID")
	private Long id;
	
	@ManyToOne
	@JoinColumn(name = "TB_PROPOSTA_ID", referencedColumnName = "ID")
	private Proposta proposta;
	
	@ManyToOne
	@JoinColumn(name = "TB_SERVICO_ID", referencedColumnName = "ID")
	private Servicos servico;
	
	@Column(name = "DATAINICIO")
	private LocalDate dataInicio;
	
	@Column(name = "DATAFIM")
	private LocalDate dataFim;
	
	@Column(name = "VALOR")
	private BigDecimal valor;
	
	@Column(name = "QUANTIDADE")
	private Integer qtd;
	
	@Column(name = "OPERACAO")
	private String operacao;
	
	@Column(name = "TOTAL")
	private BigDecimal total;
	
	@Column(name = "OBSERVACAO")
	private String obs;
	
	@Column(name = "DATAATUALIZACAO")
	private LocalDate dataAtualizacao;
	
	@PreUpdate
	public void preUpdate() {
		setDataAtualizacao(LocalDate.now());
	}
	
	@PrePersist
	public void prePersist() {
		setDataAtualizacao(LocalDate.now());
		setDataInicio(LocalDate.now());
	}
}
