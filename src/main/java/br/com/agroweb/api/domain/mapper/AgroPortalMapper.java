package br.com.agroweb.api.domain.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import br.com.agroweb.api.domain.Estado;
import br.com.agroweb.api.domain.Pessoa;
import br.com.agroweb.api.domain.Proponente;
import br.com.agroweb.api.domain.Proposta;
import br.com.agroweb.api.domain.Propriedade;
import br.com.agroweb.api.domain.Servicos;
import br.com.agroweb.api.domain.ServicosProposta;
import br.com.agroweb.api.domain.Vinculo;
import br.com.agroweb.api.domain.dto.EstadoDto;
import br.com.agroweb.api.domain.dto.PessoaDto;
import br.com.agroweb.api.domain.dto.ProponenteDto;
import br.com.agroweb.api.domain.dto.PropostaDto;
import br.com.agroweb.api.domain.dto.PropriedadeDto;
import br.com.agroweb.api.domain.dto.ServicoDto;
import br.com.agroweb.api.domain.dto.ServicoPropostaDto;
import br.com.agroweb.api.domain.dto.VinculoDto;

@Mapper(componentModel = "spring")
public interface AgroPortalMapper {
	
	@Mapping(target = "situacao", ignore = true)
	Pessoa pessoaToModel(PessoaDto pessoaDto);
	
	PessoaDto pessoaToDto(Pessoa pessoa);
	
	PropriedadeDto propriedadeToDto(Propriedade propriedade);
	
	@Mapping(target = "situacao", ignore = true)
	Propriedade propriedadeToModel(PropriedadeDto propriedadeDto);
		
	EstadoDto estadoToDto(Estado estado);
	
	Vinculo vinculoDtoToModel(VinculoDto vinculoDto);
	
	VinculoDto vinculoToDto(Vinculo vinculo);
	
	ProponenteDto proponenteToDto(Proponente proponente);
	
	Proponente proponenteDtoToModel(ProponenteDto proponenteDto);
	
	PropostaDto propostaToDto(Proposta proposta);
	
	Proposta propostaToModel(PropostaDto propostaDto);
	
	ServicoPropostaDto servicoPropostaToDto(ServicosProposta servicoProposta);
	
	ServicosProposta servicoPropostaToModel(ServicoPropostaDto servicoPropostaDto);
	
	ServicoDto servicoToDto(Servicos servico);
	
	Servicos servicoToModel(ServicoDto servicoDto);
}