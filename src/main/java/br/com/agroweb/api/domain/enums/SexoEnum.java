package br.com.agroweb.api.domain.enums;

public enum SexoEnum {

	M("Masculino"), F("Feminino");

	private String value;

	private SexoEnum(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

}
