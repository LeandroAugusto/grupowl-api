package br.com.agroweb.api.domain;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "TB_PROPOSTA")
public class Proposta {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "ID")
	private Long id;
	
	@ManyToOne
	@JoinColumn(name = "TB_PROPRIEDADE_ID", referencedColumnName = "ID")
	private Propriedade propriedade;
	
	@ManyToOne
	@JoinColumn(name = "TB_PROPONENTE_TB_PESSOA_ID", referencedColumnName = "ID")
	private Pessoa pessoa;
	
	@ManyToOne
	@JoinColumn(name = "TB_PROPONENTE_ID", referencedColumnName = "ID")
	private Proponente proponente;
	
	@Column(name = "apresentacao")
	private String apresentacao;
	
	@Column(name = "objetivo")
	private String objetivo;
	
	@Column(name = "cronogramafinanceiro")
	private String cronograma;
	
	@Column(name = "formapagamento")
	private String formaPagamento;
	
	@Column(name = "condicoesnecessarias")
	private String condicoesNecessarias;
	
	@Column(name = "localizacaoexecucao")
	private String localizacaoExecucao;
	
	@Column(name = "observacao")
	private String obs;
	
	@Column(name = "datainicio")
	private LocalDate dataInicio;
	
	@Column(name = "datafim")
	private LocalDate dataFim;
	
	@Column(name = "prazovalidade")
	private String prazoValidade;
	
	@Column(name = "dataatualizacao")
	private LocalDate dataAtualizacao;
}
