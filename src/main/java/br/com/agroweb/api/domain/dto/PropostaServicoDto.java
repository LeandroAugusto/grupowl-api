package br.com.agroweb.api.domain.dto;

import lombok.Data;

@Data
public class PropostaServicoDto {
	private PropostaDto proposta;
	private ServicoPropostaDto servicoProposta;
}
