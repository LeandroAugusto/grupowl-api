package br.com.agroweb.api.domain.dto;

import java.time.LocalDate;

import br.com.agroweb.api.domain.Estado;
import br.com.agroweb.api.domain.enums.TipoPessoaEnum;
import lombok.Data;

@Data
public class PessoaDto {
	private Long id;
	private Estado estado;
	private String cidade;
	private String bairro;
	private String cpfCnpj;
	private String nomeRazaoSocial;
	private TipoPessoaEnum tipoPessoa;
	private String sexo;
	private String estadoCivil;
	private String naturalidade;
	private String nacionalidade;
	private String logradouro;
	private String complemento;
	private String numero;
	private String telefone;
	private String celularUm;
	private String celularDois;
	private String celularWpp;
	private String email;
	private LocalDate dataNascimento;
	private String cep;
	private String obs;
}
