package br.com.agroweb.api.domain.dto;

import java.time.LocalDate;

import lombok.Data;

@Data
public class ServicoDto {
	private Long id;
	private String nome;
	private String descricao;
	private Integer situacao;
	private LocalDate dataAtualizacao;
}
