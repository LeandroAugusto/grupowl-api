package br.com.agroweb.api.domain.dto;

import br.com.agroweb.api.domain.Pessoa;
import br.com.agroweb.api.domain.Propriedade;
import lombok.Data;

@Data
public class VinculoDto {

	private Long id;
	private Propriedade propriedade;
	private Pessoa pessoa;
	private String tpVinculo;
}
