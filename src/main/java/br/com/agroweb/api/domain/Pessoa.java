package br.com.agroweb.api.domain;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

import br.com.agroweb.api.domain.enums.TipoPessoaEnum;
import lombok.Data;

@Data
@Entity
@Table(name = "TB_PESSOA")
public class Pessoa {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "ID")
	private Long id;
	
	@ManyToOne
	@JoinColumn(name = "tb_bairro_tb_cidade_tb_estado_id", referencedColumnName = "ID")
	private Estado estado;
	
	@Column(name = "cidade")
	private String cidade;
	
	@Column(name = "bairro")
	private String bairro;
	
	@Column(name = "cpfcnpj")
	private String cpfCnpj;
	
	@Column(name = "nomerazaosocial")
	private String nomeRazaoSocial;
	
	@Column(name = "tipopessoa")
	@Enumerated(EnumType.STRING)
	private TipoPessoaEnum tipoPessoa;
	
	@Column(name = "sexo")
	private String sexo;
	
	@Column(name = "estadocivil")
	private String estadoCivil;
	
	@Column(name = "naturalidade")
	private String naturalidade;
	
	@Column(name = "nacionalidade")
	private String nacionalidade;
	
	@Column(name = "logradouro")
	private String logradouro;
	
	@Column(name = "complemento")
	private String complemento;
	
	@Column(name = "numero")
	private String numero;
	
	@Column(name = "telefone")
	private String telefone;
	
	@Column(name = "celularum")
	private String celularUm;
	
	@Column(name = "celulardois")
	private String celularDois;
	
	@Column(name = "celularwpp")
	private String celularWpp;
	
	@Column(name = "email")
	private String email;
	
	@Column(name = "datanascimento")
	private LocalDate dataNascimento;
	
	@Column(name = "cep")
	private String cep;
	
	@Column(name = "observacoes")
	private String obs;
	
	@Column(name = "situacao")
	private Integer situacao;

	public void situacaoAtiva() {
		this.situacao = 1;
	}
	
	public void situacaoInativa() {
		this.situacao = 0;
	}
	
	@PreUpdate
	public void preUpdate() {
		this.situacaoAtiva();
	}
	
	@PrePersist
	public void prePersist() {
		this.situacaoAtiva();
	}
}