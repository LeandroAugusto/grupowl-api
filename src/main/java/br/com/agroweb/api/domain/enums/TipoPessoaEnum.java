package br.com.agroweb.api.domain.enums;

public enum TipoPessoaEnum {

	PF("Pessoa Física"), PJ("Pessoa Jurídica");

	private String value;

	private TipoPessoaEnum(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
}
