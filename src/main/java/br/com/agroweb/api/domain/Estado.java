package br.com.agroweb.api.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "TB_ESTADO")
public class Estado {

	@Id
	@SequenceGenerator(name = "seqEstado", sequenceName = "tb_estado_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqEstado")
	@Column(name = "ID")
	private Long id;

	@Column(name = "NOME")
	private String nome;

	@Column(name = "SIGLA")
	private String sigla;

	@Column(name = "SITUACAO")
	private Integer situacao;

	public void situacaoAtiva() {
		this.situacao = 1;
	}
	
	public void situacaoInativa() {
		this.situacao = 0;
	}
}
