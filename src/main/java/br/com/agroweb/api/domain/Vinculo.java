package br.com.agroweb.api.domain;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "TB_VINCULO")
public class Vinculo {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "ID")
	private Long id;
	
	@ManyToOne
	@JoinColumn(name = "TB_PROPRIEDADE_ID", referencedColumnName = "ID")
	private Propriedade propriedade;
	
	@ManyToOne
	@JoinColumn(name = "TB_PESSOA_ID", referencedColumnName = "ID")
	private Pessoa pessoa;
	
	@Column(name = "TIPOVINCULO")
	private String tpVinculo;

	@Column(name = "DATAATUALIZACAO")
	private LocalDate dataAtualizacao;
	
	@PrePersist
	public void preSalvar() {
		setDataAtualizacao(LocalDate.now());
	}
	
	@PreUpdate
	public void preAtualizar() {
		setDataAtualizacao(LocalDate.now());
	}
}
