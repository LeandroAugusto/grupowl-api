package br.com.agroweb.api.domain.dto;

import java.time.LocalDate;

import br.com.agroweb.api.domain.Estado;
import br.com.agroweb.api.domain.Pessoa;
import br.com.agroweb.api.domain.enums.SexoEnum;
import br.com.agroweb.api.domain.enums.TipoPessoaEnum;
import lombok.Data;

@Data
public class ProponenteDto {
	private Long id;
	private Pessoa pessoa;
	private Estado estado;
	private String cidade;
	private String bairro;
	private String nome;
	private String cpfCnpj;
	private TipoPessoaEnum tipoPessoa;
	private SexoEnum sexo;
	private String estadoCivil;
	private String naturalidade;
	private String nacionalidade;
	private String logradouro;
	private String complemento;
	private String numero;
	private String telefone;
	private String celularUm;
	private String celularDois;
	private String celularWpp;
	private String email;
	private LocalDate dataNascimento;
	private String cep;
	private String obs;
	private Integer situacao;
}
