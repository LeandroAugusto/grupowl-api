package br.com.agroweb.api.domain;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;

import br.com.agroweb.api.domain.enums.SexoEnum;
import br.com.agroweb.api.domain.enums.TipoPessoaEnum;
import lombok.Data;

@Data
@Entity
@Table(name = "TB_PROPONENTE")
public class Proponente {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "ID")
	private Long id;
	
	@ManyToOne
	@JoinColumn(name = "TB_PESSOA_ID", referencedColumnName = "ID")
	private Pessoa pessoa;
	
	@ManyToOne
	@JoinColumn(name = "ID_ESTADO", referencedColumnName = "ID")
	private Estado estado;
	
	@Column(name = "CIDADE")
	private String cidade;

	@Column(name = "BAIRRO")
	private String bairro;
	
	@Column(name = "NOMERAZAOSOCIAL")
	private String nome;

	@Column(name = "CPFCNPJ")
	private String cpfCnpj;

	@Enumerated(EnumType.STRING)
	@Column(name = "TIPOPESSOA")
	private TipoPessoaEnum tipoPessoa;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "SEXO")
	private SexoEnum sexo;
	
	@Column(name = "estadocivil")
	private String estadoCivil;
	
	@Column(name = "naturalidade")
	private String naturalidade;
	
	@Column(name = "nacionalidade")
	private String nacionalidade;
	
	@Column(name = "logradouro")
	private String logradouro;
	
	@Column(name = "complemento")
	private String complemento;
	
	@Column(name = "numero")
	private String numero;
	
	@Column(name = "telefone")
	private String telefone;
	
	@Column(name = "celularum")
	private String celularUm;
	
	@Column(name = "celulardois")
	private String celularDois;
	
	@Column(name = "celularwpp")
	private String celularWpp;
	
	@Column(name = "email")
	private String email;
	
	@Column(name = "datanascimento")
	private LocalDate dataNascimento;
	
	@Column(name = "cep")
	private String cep;
	
	@Column(name = "observacoes")
	private String obs;
	
	@Column(name = "situacao")
	private Integer situacao;
	
	@PrePersist
	public void prePersist() {
		setSituacao(1);
	}
}