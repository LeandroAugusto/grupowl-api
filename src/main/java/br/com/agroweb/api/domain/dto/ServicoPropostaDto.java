package br.com.agroweb.api.domain.dto;

import java.math.BigDecimal;
import java.time.LocalDate;

import br.com.agroweb.api.domain.Proposta;
import br.com.agroweb.api.domain.Servicos;
import lombok.Data;

@Data
public class ServicoPropostaDto {
	private Long id;
	private Proposta proposta;
	private Servicos servico;
	private LocalDate dataInicio;
	private LocalDate dataFim;
	private BigDecimal valor;
	private Integer qtd;
	private String operacao;
	private BigDecimal total;
	private String obs;
}
