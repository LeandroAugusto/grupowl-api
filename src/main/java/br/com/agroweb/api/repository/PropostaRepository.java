package br.com.agroweb.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.agroweb.api.domain.Proposta;

@Repository
public interface PropostaRepository extends JpaRepository<Proposta, Long>{

	@Query("SELECT p FROM Proposta p WHERE "
			+ ":cpfCnpj IS NULL OR p.pessoa.cpfCnpj = :cpfCnpj")
	List<Proposta> findByCpfCnpj(@Param("cpfCnpj") String cpfCnpj);
}
