package br.com.agroweb.api.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.agroweb.api.domain.Propriedade;

@Repository
public interface PropriedadeRepository extends JpaRepository<Propriedade, Long>{
	
	@Query("SELECT p FROM Propriedade p WHERE p.situacao = 1"
			+ " ORDER BY p.nome")
	List<Propriedade> findByFilters();
	
	Optional<Propriedade> findByCpfCnpj(String cpfCnpj);
}
