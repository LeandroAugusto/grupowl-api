package br.com.agroweb.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.agroweb.api.domain.Vinculo;

@Repository
public interface VinculoRepository extends JpaRepository<Vinculo, Long>{
	List<Vinculo> findByPessoaCpfCnpj(String cpfCnpj);
	
	List<Vinculo> findByPropriedadeId(Long id);
}