package br.com.agroweb.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.agroweb.api.domain.Estado;

@Repository
public interface EstadoRepository extends JpaRepository<Estado, Long>{
	Estado findBySigla(String sigla);
}
