package br.com.agroweb.api.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.agroweb.api.domain.Proponente;

@Repository
public interface ProponenteRepository extends JpaRepository<Proponente, Long>{
	
	Optional<Proponente> findByCpfCnpj(String cpfCnpj);
}
