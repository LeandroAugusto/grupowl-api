package br.com.agroweb.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.agroweb.api.domain.Servicos;

@Repository
public interface ServicoRepository extends JpaRepository<Servicos, Long>{

}
