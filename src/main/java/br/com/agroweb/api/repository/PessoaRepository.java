package br.com.agroweb.api.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.agroweb.api.domain.Pessoa;

@Repository
public interface PessoaRepository extends JpaRepository<Pessoa, Long>{
	
	@Query("SELECT p FROM Pessoa p WHERE p.situacao = 1"
			+ " ORDER BY p.nomeRazaoSocial")
	List<Pessoa> findByFilters();
	
	Optional<Pessoa> findByCpfCnpj(String cpfCnpj);
}	
