package br.com.agroweb.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.agroweb.api.domain.ServicosProposta;

@Repository
public interface ServicoPropostaRepository extends JpaRepository<ServicosProposta, Long>{
	
	List<ServicosProposta> findByPropostaId(Long id);
	
}
