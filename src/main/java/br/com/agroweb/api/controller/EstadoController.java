package br.com.agroweb.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.agroweb.api.domain.Estado;
import br.com.agroweb.api.service.EstadoService;

@RestController
@CrossOrigin
@RequestMapping("v1/api/estado")
public class EstadoController {
	
	@Autowired
	private EstadoService service;
	
	@GetMapping("{sigla}")
	public ResponseEntity<Estado> findBySigla(@PathVariable("sigla") String sigla){
		return ResponseEntity.ok(service.getBySigla(sigla));
	}
	
	@GetMapping()
	public ResponseEntity<List<Estado>> findAll(){
		return ResponseEntity.ok(service.getAll());
	}
}
