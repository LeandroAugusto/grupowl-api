package br.com.agroweb.api.controller;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.agroweb.api.domain.dto.PessoaDto;
import br.com.agroweb.api.domain.dto.PropriedadeDto;
import br.com.agroweb.api.domain.dto.VinculoDto;
import br.com.agroweb.api.service.PropriedadeService;

@RestController
@CrossOrigin
@RequestMapping("v1/api/propriedade")
public class PropriedadeController {
	
	@Autowired
	private PropriedadeService service;
	
	@GetMapping("{id}")
	public ResponseEntity<PropriedadeDto> findById(@PathVariable("id") Long id){
		return ResponseEntity.ok(service.getById(id));
	}
	
	@GetMapping("cpfCnpj/{cpfCnpj}")
	public ResponseEntity<PropriedadeDto> findByCpfCnpj(@PathVariable("cpfCnpj") String cpfCnpj){
		return ResponseEntity.ok(service.getByCpfCnpj(cpfCnpj));
	}
	
	@GetMapping("pessoa/{cpfCnpj}")
	public ResponseEntity<List<PropriedadeDto>> findByPessoaId(@PathVariable("cpfCnpj") String cpfCnpj){
		return ResponseEntity.ok(service.findByPessoa(cpfCnpj));
	}
	
	@GetMapping()
	public ResponseEntity<List<PropriedadeDto>> findByFilters(@RequestParam(required = false) String nomeRazaoSocial, @RequestParam(required = false) String cpfCnpj){
		return ResponseEntity.ok(service.getByFilter(nomeRazaoSocial, cpfCnpj));
	}
	
	@PostMapping()
	public ResponseEntity<PropriedadeDto> save(@Valid @RequestBody PropriedadeDto propriedadeDto){
		PropriedadeDto registroSalvo = service.create(propriedadeDto);
        URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{id}").buildAndExpand(registroSalvo.getId()).toUri();
        return ResponseEntity.created(uri).body(registroSalvo);
	}
	
	@PostMapping("vinculo")
	public ResponseEntity<VinculoDto> saveVinculo(@Valid @RequestBody VinculoDto vinculoDto){
		VinculoDto registroSalvo = service.createVinculo(vinculoDto);
        URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{id}").buildAndExpand(registroSalvo.getId()).toUri();
        return ResponseEntity.created(uri).body(registroSalvo);
	}
	
	@GetMapping("vinculo/{id}")
	public ResponseEntity<List<PessoaDto>> findByPropriedadeId(@PathVariable("id") Long id){
		return ResponseEntity.ok(service.getbyPropriedadeId(id));
	}
	
	@PutMapping("{id}")
	public ResponseEntity<PropriedadeDto> update(@PathVariable("id") Long id, @Valid @RequestBody PropriedadeDto propriedadeDto){
		return ResponseEntity.ok(service.update(id, propriedadeDto));
	}
	
	@DeleteMapping("{id}")
	public ResponseEntity<Void> delete(@PathVariable("id") Long id){
		service.delete(id);
		return ResponseEntity.noContent().build();
	}
}	