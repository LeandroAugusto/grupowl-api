package br.com.agroweb.api.controller;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.agroweb.api.domain.dto.PessoaDto;
import br.com.agroweb.api.domain.dto.ProponenteDto;
import br.com.agroweb.api.domain.enums.TipoPessoaEnum;
import br.com.agroweb.api.service.impl.PessoaServiceImpl;
import br.com.agroweb.api.service.impl.ProponenteServiceImpl;

@RestController
@CrossOrigin
@RequestMapping("v1/api/pessoa")
public class PessoaController {
	
	@Autowired
	private PessoaServiceImpl service;
	
	@Autowired
	private ProponenteServiceImpl proponenteService;
	
	@GetMapping("{id}")
	public ResponseEntity<PessoaDto> findById(@PathVariable("id") Long id){
		return ResponseEntity.ok(service.getById(id));
	}
	
	@GetMapping("proponente/{id}")
	public ResponseEntity<ProponenteDto> findProponenteById(@PathVariable("id") Long id){
		return ResponseEntity.ok(proponenteService.getById(id));
	}
	
	@GetMapping("proponente/cpfCnpj/{cpfCnpj}")
	public ResponseEntity<ProponenteDto> findProponenteByCpfCnpj(@PathVariable("cpfCnpj") String cpfCnpj){
		return ResponseEntity.ok(proponenteService.getByCpfCnpj(cpfCnpj));
	}
	
	@GetMapping("cpfCnpj/{cpfCnpj}")
	public ResponseEntity<PessoaDto> findByCpfCnpj(@PathVariable("cpfCnpj") String cpfCnpj){
		return ResponseEntity.ok(service.getByCpfCnpj(cpfCnpj));
	}
	
	@GetMapping("buscar")
	public ResponseEntity<List<PessoaDto>> findByFilters(@RequestParam(required = false) String nomeRazaoSocial, 
													  @RequestParam(required = false) String cpfCnpj, 
													  @RequestParam(required = false) TipoPessoaEnum tipoPessoa){
		return ResponseEntity.ok(service.getByFilter(nomeRazaoSocial, cpfCnpj, tipoPessoa));
	}
	
	@PostMapping("salvar")
	public ResponseEntity<PessoaDto> save(@Valid @RequestBody PessoaDto pessoaDto){
		PessoaDto registroSalvo = service.create(pessoaDto);
        URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{id}").buildAndExpand(registroSalvo.getId()).toUri();
        return ResponseEntity.created(uri).body(registroSalvo);
	}
	
	@PostMapping("salvar/proponente")
	public ResponseEntity<ProponenteDto> saveProponente(@Valid @RequestBody ProponenteDto proponenteDto){
		ProponenteDto registroSalvo = proponenteService.create(proponenteDto);
        URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{id}").buildAndExpand(registroSalvo.getId()).toUri();
        return ResponseEntity.created(uri).body(registroSalvo);
	}
	
	@PutMapping("atualizar/{id}")
	public ResponseEntity<PessoaDto> update(@PathVariable("id") Long id, @Valid @RequestBody PessoaDto pessoaDto){
		return ResponseEntity.ok(service.update(id, pessoaDto));
	}
	
	@DeleteMapping("deletar/{id}")
	public ResponseEntity<Void> delete(@PathVariable("id") Long id){
		service.delete(id);
		return ResponseEntity.noContent().build();
	}
}
