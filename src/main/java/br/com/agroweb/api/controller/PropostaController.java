package br.com.agroweb.api.controller;

import java.io.IOException;
import java.net.URI;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.itextpdf.text.DocumentException;

import br.com.agroweb.api.domain.dto.PropostaDto;
import br.com.agroweb.api.domain.dto.ServicoDto;
import br.com.agroweb.api.domain.dto.ServicoPropostaDto;
import br.com.agroweb.api.service.PropostaService;
import br.com.agroweb.api.service.ServicoPropostaService;
import br.com.agroweb.api.utils.GerarPdfUtils;

@RestController
@CrossOrigin
@RequestMapping("v1/api/proposta")
public class PropostaController {
	
	private PropostaDto proposta;
	
	@Autowired
	private PropostaService service;
	
	@Autowired
	private ServicoPropostaService servicoService;
	
	@GetMapping()
	public ResponseEntity<List<PropostaDto>> findByFIlters(@RequestParam(required = false) String cpfCnpj){
		return ResponseEntity.ok(service.findByFilters(cpfCnpj));
	}
	
	@GetMapping("{id}")
	public ResponseEntity<PropostaDto> findById(@PathVariable("id") Long id){
		return ResponseEntity.ok(service.findById(id));
	}
	
	@GetMapping("{id}/servico")
	public ResponseEntity<List<ServicoPropostaDto>> findByProposta(@PathVariable("id") Long id){
		return ResponseEntity.ok(servicoService.findByProposta(id));
	}
	
	@PostMapping("salvar")
	public ResponseEntity<PropostaDto> save(@Valid @RequestBody PropostaDto propostaDto){
		this.proposta = new PropostaDto();
		PropostaDto registroSalvo = service.create(propostaDto);
		this.proposta = registroSalvo;
        URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{id}").buildAndExpand(registroSalvo.getId()).toUri();
        return ResponseEntity.created(uri).body(registroSalvo);
	}
	
	@GetMapping("servico")
	public ResponseEntity<List<ServicoDto>> findAllService(){
		return ResponseEntity.ok(servicoService.getAllServico());
	}
	
	@GetMapping(value = "gerar-contrato", produces = MediaType.APPLICATION_PDF_VALUE)
	public ResponseEntity<byte[]> gerarContrato(HttpServletResponse response) throws DocumentException, IOException{
		byte[] bytes = new GerarPdfUtils().gerarContratoProposta(this.proposta);
		
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add(HttpHeaders.CONTENT_DISPOSITION, "filename=Proposta.pdf");
		response.setHeader("Content-Disposition", "attachment; filename=Proposta.pdf");
		
		ServletOutputStream ouputStream = response.getOutputStream();
		try {			
			ouputStream.write(bytes, 0, bytes.length);
			ouputStream.flush();
			ouputStream.close();
		} catch (IOException e) {
			throw e;
		}

		return ResponseEntity.ok().headers(httpHeaders).body(bytes);
	}
}