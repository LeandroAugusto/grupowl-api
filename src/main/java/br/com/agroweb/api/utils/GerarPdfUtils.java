package br.com.agroweb.api.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;

import javax.imageio.ImageIO;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import br.com.agroweb.api.domain.dto.PropostaDto;
import br.com.agroweb.api.domain.dto.ServicoPropostaDto;

public class GerarPdfUtils {

	public static final Font BOLD = new Font(FontFamily.TIMES_ROMAN, 12, Font.BOLD);
	public static final Font NORMAL = new Font(FontFamily.TIMES_ROMAN, 12);

	public byte[] gerarContratoProposta(PropostaDto proposta) throws DocumentException, IOException {

		URL image = getClass().getResource("/images/logo-agroportal.png");
		URL image2 = getClass().getResource("/images/rodape.png");

		Document document = new Document();
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		PdfWriter.getInstance(document, byteArrayOutputStream);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ByteArrayOutputStream baos2 = new ByteArrayOutputStream();
		
		document.open();

		ImageIO.write(ImageIO.read(image), "png", baos);
		Image img = Image.getInstance(baos.toByteArray());
		img.setAlignment(Element.ALIGN_MIDDLE);
		document.add(img);

		Paragraph p1 = new Paragraph();
		p1.add(new Phrase("PROPOSTA DE PRESTAÇÃO DE SERVIÇO", BOLD));
		p1.setAlignment(Element.ALIGN_JUSTIFIED);
		p1.add("                                                              ");
		p1.setSpacingBefore(25);
		p1.setSpacingAfter(25);
		p1.add(new DateConvertUtils().convertToString(LocalDate.now()));
		document.add(p1);

		Paragraph p2 = new Paragraph();
		p2.add(new Phrase("Ao Sr(a) " + proposta.getPessoa().getNomeRazaoSocial(), BOLD));
		p2.setAlignment(Element.ALIGN_JUSTIFIED);
		p2.setSpacingAfter(25);
		document.add(p2);

		Paragraph p3 = new Paragraph();
		p3.add("Proposta de Número: " + proposta.getId() + " - AGROPORTAL");
		p3.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(p3);

		Paragraph p4 = new Paragraph();
		p4.add(new Phrase("1 - APRESENTAÇÃO", BOLD));
		p4.setAlignment(Element.ALIGN_JUSTIFIED);
		p4.setSpacingBefore(10);
		document.add(p4);

		Paragraph p5 = new Paragraph();
		p5.add(proposta.getApresentacao());
		p5.setAlignment(Element.ALIGN_JUSTIFIED);
		p5.setFirstLineIndent(18);
		document.add(p5);

		Paragraph p6 = new Paragraph();
		p6.add(new Phrase("2 - OBJETIVO", BOLD));
		p6.setAlignment(Element.ALIGN_JUSTIFIED);
		p6.setSpacingBefore(10);
		document.add(p6);

		Paragraph p7 = new Paragraph();
		p7.add(proposta.getObjetivo());
		p7.setAlignment(Element.ALIGN_JUSTIFIED);
		p7.setFirstLineIndent(18);
		document.add(p7);

		Paragraph p8 = new Paragraph();
		p8.add(new Phrase("3 - CONDIÇÕES NECESSÁRIAS", BOLD));
		p8.setAlignment(Element.ALIGN_JUSTIFIED);
		p8.setSpacingBefore(10);
		document.add(p8);

		Paragraph p9 = new Paragraph();
		p9.add(proposta.getCondicoesNecessarias());
		p9.setAlignment(Element.ALIGN_JUSTIFIED);
		p9.setFirstLineIndent(18);
		document.add(p9);

		Paragraph p10 = new Paragraph();
		p10.add(new Phrase("4 - LOCALIZAÇÃO EXECUÇÃO", BOLD));
		p10.setAlignment(Element.ALIGN_JUSTIFIED);
		p10.setSpacingBefore(10);
		document.add(p10);

		Paragraph p11 = new Paragraph();
		p11.add(proposta.getLocalizacaoExecucao());
		p11.setAlignment(Element.ALIGN_JUSTIFIED);
		p11.setFirstLineIndent(18);
		document.add(p11);

		Paragraph p12 = new Paragraph();
		p12.add(new Phrase("5 - VALORES E SERVIÇOS PROPOSTOS", BOLD));
		p12.setAlignment(Element.ALIGN_JUSTIFIED);
		p12.setSpacingBefore(10);
		document.add(p12);

		PdfPTable table = new PdfPTable(2);
		table.setWidthPercentage(100); // Width 100%
		table.setSpacingBefore(10f); // Space before table
		table.setSpacingAfter(10f); // Space after table

		float[] columnWidths = { 1f, 1f };
		table.setWidths(columnWidths);

		PdfPCell cell1 = new PdfPCell(new Paragraph("SERVIÇO"));
		cell1.setPaddingLeft(10);
		cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);
		table.addCell(cell1);
		
		PdfPCell cell2 = new PdfPCell(new Paragraph("VALORES"));
		cell2.setPaddingLeft(10);
		cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell2.setVerticalAlignment(Element.ALIGN_MIDDLE);
		table.addCell(cell2);
		
		for(ServicoPropostaDto serv : proposta.getServicosPropostas()) {
			table.addCell(serv.getServico().getNome());
			table.addCell(serv.getValor().toString());
		}
		document.add(table);
		
		Paragraph p13 = new Paragraph();
		p13.add(new Phrase("6 - FORMA DE PAGAMENTO", BOLD));
		p13.setAlignment(Element.ALIGN_JUSTIFIED);
		p13.setSpacingBefore(10);
		document.add(p13);

		Paragraph p14 = new Paragraph();
		p14.add(proposta.getFormaPagamento());
		p14.setAlignment(Element.ALIGN_JUSTIFIED);
		p14.setFirstLineIndent(18);
		document.add(p14);
		
		Paragraph p15 = new Paragraph();
		p15.add(new Phrase("7 - PRAZO DE VALIDADE DA PROPOSTA", BOLD));
		p15.setAlignment(Element.ALIGN_JUSTIFIED);
		p15.setSpacingBefore(10);
		document.add(p15);

		Paragraph p16 = new Paragraph();
		p16.add(proposta.getPrazoValidade());
		p16.setAlignment(Element.ALIGN_JUSTIFIED);
		p16.setFirstLineIndent(18);
		document.add(p16);
		
		Paragraph p17 = new Paragraph();
		p17.add(new Phrase("8 - OBSERVAÇÕES", BOLD));
		p17.setAlignment(Element.ALIGN_JUSTIFIED);
		p17.setSpacingBefore(10);
		document.add(p17);

		Paragraph p18 = new Paragraph();
		p18.add(proposta.getObs());
		p18.setAlignment(Element.ALIGN_JUSTIFIED);
		p18.setFirstLineIndent(18);
		document.add(p18);
		
		ImageIO.write(ImageIO.read(image2), "png", baos2);
		Image img2 = Image.getInstance(baos2.toByteArray());
		img2.setAlignment(Element.ALIGN_MIDDLE);
		img2.scalePercent(50, 50);
		document.add(img2); 
		
		document.close(); 

		return byteArrayOutputStream.toByteArray();
	}
}