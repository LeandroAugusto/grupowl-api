package br.com.agroweb.api.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.CONFLICT)
public class AgroWebException extends RuntimeException{
	
	private static final long serialVersionUID = -1304081848634371217L;

	public AgroWebException(String message) {
		super(message);
	}
}
