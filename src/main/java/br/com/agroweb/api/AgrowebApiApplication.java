package br.com.agroweb.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableCaching
@EnableScheduling
@SpringBootApplication
@EnableFeignClients
public class AgrowebApiApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(AgrowebApiApplication.class, args);
	}
}
