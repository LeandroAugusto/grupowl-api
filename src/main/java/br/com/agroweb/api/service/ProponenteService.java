package br.com.agroweb.api.service;

import br.com.agroweb.api.domain.dto.ProponenteDto;

public interface ProponenteService {
	
	ProponenteDto create(ProponenteDto proponenteDto);
	
	ProponenteDto getById(Long id);
	
	ProponenteDto getByCpfCnpj(String cpfCnpj);
}
