package br.com.agroweb.api.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.agroweb.api.domain.Estado;
import br.com.agroweb.api.exception.AgroWebException;
import br.com.agroweb.api.repository.EstadoRepository;
import br.com.agroweb.api.service.EstadoService;

@Service
public class EstadoServiceImpl implements EstadoService{
	
	private EstadoRepository estadoRepo;
	
	@Autowired
	public EstadoServiceImpl(EstadoRepository estadoRepo) {
		this.estadoRepo = estadoRepo;
	}
	
	public Estado findById(Long id) {
		return estadoRepo.findById(id).filter(item -> item.getSituacao() != 0).orElseThrow(() -> new AgroWebException("Estado não localizado"));
	}
	
	public Estado save(Estado estado) {
		estado.situacaoAtiva();
		return estadoRepo.save(estado);
	}
	
	public Estado update(Long id, Estado estado) {
		Estado estadoLocalizado = findById(id);
		BeanUtils.copyProperties(estadoLocalizado, estado, "id");
		return estadoRepo.save(estadoLocalizado);
	}
	
	public void delete(Long id) {
		Estado estado = findById(id);
		estado.situacaoInativa();;
		estadoRepo.save(estado);
	}

	@Override
	public Estado getBySigla(String sigla) {
		return estadoRepo.findBySigla(sigla);
	}
	
	@Override
	public List<Estado> getAll(){
		return estadoRepo.findAll().stream().filter(item -> item.getSituacao() != 0).collect(Collectors.toList());
	}
}
