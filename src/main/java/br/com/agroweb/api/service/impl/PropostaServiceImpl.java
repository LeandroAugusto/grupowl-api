package br.com.agroweb.api.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.agroweb.api.domain.Proposta;
import br.com.agroweb.api.domain.dto.PropostaDto;
import br.com.agroweb.api.domain.dto.ServicoPropostaDto;
import br.com.agroweb.api.domain.mapper.AgroPortalMapper;
import br.com.agroweb.api.repository.PropostaRepository;
import br.com.agroweb.api.service.PropostaService;

@Service
public class PropostaServiceImpl implements PropostaService{
	
	@Autowired
	private AgroPortalMapper mapper;
	
	@Autowired
	private ServicoPropostaServiceImpl servicoProposta;

	private PropostaRepository propostaRepo;

	@Autowired
	public PropostaServiceImpl(PropostaRepository propostaRepo) {
		this.propostaRepo = propostaRepo;
	}
	
	@Override
	public PropostaDto create(PropostaDto propostaDto) {
		List<ServicoPropostaDto> lista = new ArrayList<>();
		Proposta proposta = propostaRepo.save(mapper.propostaToModel(propostaDto));
		for(ServicoPropostaDto serv : propostaDto.getServicosPropostas()) {
			serv.setProposta(proposta);
			lista.add(servicoProposta.create(serv));
		}
		PropostaDto propostaSalva = mapper.propostaToDto(proposta);
		propostaSalva.setServicosPropostas(lista);
		return propostaSalva;
	}

	@Override
	public List<PropostaDto> findByFilters(String cpfCnpj) {
		List<Proposta> lista = propostaRepo.findByCpfCnpj(cpfCnpj);
		return lista.stream().map(item -> mapper.propostaToDto(item)).collect(Collectors.toList());
	}

	@Override
	public PropostaDto findById(Long id) {
		return mapper.propostaToDto(propostaRepo.findById(id).orElse(null));
	}
}