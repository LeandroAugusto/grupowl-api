package br.com.agroweb.api.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.agroweb.api.domain.Propriedade;
import br.com.agroweb.api.domain.Vinculo;
import br.com.agroweb.api.domain.dto.PessoaDto;
import br.com.agroweb.api.domain.dto.PropriedadeDto;
import br.com.agroweb.api.domain.dto.VinculoDto;
import br.com.agroweb.api.domain.mapper.AgroPortalMapper;
import br.com.agroweb.api.repository.PropriedadeRepository;
import br.com.agroweb.api.repository.VinculoRepository;
import br.com.agroweb.api.service.PropriedadeService;

@Service
public class PropriedadeServiceImpl implements PropriedadeService {

	private PropriedadeRepository propriedadeRepo;

	private VinculoRepository vinculoRepo;

	@Autowired
	private AgroPortalMapper mapper;

	@Autowired
	public PropriedadeServiceImpl(PropriedadeRepository propriedadeRepo, VinculoRepository vinculoRepo) {
		this.propriedadeRepo = propriedadeRepo;
		this.vinculoRepo = vinculoRepo;
	}

	@Override
	public PropriedadeDto getById(Long id) {
		Optional<Propriedade> propriedade = propriedadeRepo.findById(id);
		return mapper.propriedadeToDto(propriedade.orElse(null));
	}

	@Override
	public List<PropriedadeDto> getByFilter(String nomeRazaoSocial, String cpfCnpj) {
		List<Propriedade> listPropriedade = propriedadeRepo.findByFilters();
		if(nomeRazaoSocial != null) {
			listPropriedade = propriedadeRepo.findByFilters()
					.stream().filter(item -> nomeRazaoSocial.equalsIgnoreCase(item.getNome()))
					.collect(Collectors.toList());
		}
		if(cpfCnpj != null) {
			listPropriedade = propriedadeRepo.findByFilters()
					.stream().filter(item -> cpfCnpj.equals(item.getCpfCnpj()))
					.collect(Collectors.toList());
		}
		return listPropriedade.stream().map(propriedade -> mapper.propriedadeToDto(propriedade))
				.collect(Collectors.toList());
	}

	@Override
	public PropriedadeDto create(PropriedadeDto propriedadeDto) {
		Propriedade propriedade = mapper.propriedadeToModel(propriedadeDto);
		return mapper.propriedadeToDto(propriedadeRepo.save(propriedade));
	}

	@Override
	public VinculoDto createVinculo(VinculoDto vinculoDto) {
		Vinculo vinculo = mapper.vinculoDtoToModel(vinculoDto);
		return mapper.vinculoToDto(vinculoRepo.save(vinculo));
	}

	@Override
	public PropriedadeDto update(Long id, PropriedadeDto propriedadeDto) {
		PropriedadeDto propriedade = getById(id);
		BeanUtils.copyProperties(propriedadeDto, propriedade, "id");
		return mapper.propriedadeToDto(propriedadeRepo.save(mapper.propriedadeToModel(propriedade)));
	}

	@Override
	public void delete(Long id) {
		propriedadeRepo.deleteById(id);
	}

	@Override
	public PropriedadeDto getByCpfCnpj(String cpfCnpj) {
		Optional<Propriedade> propriedade = propriedadeRepo.findByCpfCnpj(cpfCnpj);
		return mapper.propriedadeToDto(propriedade.orElse(null));
	}

	@Override
	public List<PropriedadeDto> findByPessoa(String cpfCnpj) {
		List<Vinculo> vinculos = vinculoRepo.findByPessoaCpfCnpj(cpfCnpj);
		List<PropriedadeDto> lista = new ArrayList<>();
		
		vinculos.forEach(item -> {
			lista.add(mapper.propriedadeToDto(item.getPropriedade()));
		});
		return lista;
	}

	@Override
	public List<PessoaDto> getbyPropriedadeId(Long id) {
		List<Vinculo> vinculos = vinculoRepo.findByPropriedadeId(id);
		List<PessoaDto> lista = new ArrayList<>();
		vinculos.forEach(item -> {
			lista.add(mapper.pessoaToDto(item.getPessoa()));
		});
		return lista;
	}
}