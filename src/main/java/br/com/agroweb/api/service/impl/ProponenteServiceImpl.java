package br.com.agroweb.api.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.agroweb.api.domain.Proponente;
import br.com.agroweb.api.domain.dto.ProponenteDto;
import br.com.agroweb.api.domain.mapper.AgroPortalMapper;
import br.com.agroweb.api.repository.ProponenteRepository;
import br.com.agroweb.api.service.ProponenteService;

@Service
public class ProponenteServiceImpl implements ProponenteService{
	
	@Autowired
	private AgroPortalMapper mapper;

	private ProponenteRepository proponenteRepo;

	@Autowired
	public ProponenteServiceImpl(ProponenteRepository proponenteRepo) {
		this.proponenteRepo = proponenteRepo;
	}
	
	@Override
	public ProponenteDto create(ProponenteDto proponenteDto) {
		Proponente proponente = mapper.proponenteDtoToModel(proponenteDto);
		return mapper.proponenteToDto(proponenteRepo.save(proponente));
	}

	@Override
	public ProponenteDto getById(Long id) {
		Optional<Proponente> proponente = proponenteRepo.findById(id);
		return mapper.proponenteToDto(proponente.orElse(null));
	}
	
	@Override
	public ProponenteDto getByCpfCnpj(String cpfCnpj) {
		Optional<Proponente> proponente = proponenteRepo.findByCpfCnpj(cpfCnpj);
		return mapper.proponenteToDto(proponente.orElse(null));
	}
}
