package br.com.agroweb.api.service;

import java.util.List;

import br.com.agroweb.api.domain.dto.PessoaDto;
import br.com.agroweb.api.domain.enums.TipoPessoaEnum;

public interface PessoaService {
	
	PessoaDto getById(Long id);
	
	PessoaDto getByCpfCnpj(String cpfCnpj);
	
	List<PessoaDto> getByFilter(String nomeRazaoSocial, String cpfCnpj, TipoPessoaEnum tipoPessoa);
	
	PessoaDto create(PessoaDto pessoaDto);
	
	PessoaDto update(Long id, PessoaDto pessoaDto);
	
	void delete(Long id);
}
