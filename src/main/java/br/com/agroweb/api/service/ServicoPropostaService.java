package br.com.agroweb.api.service;

import java.util.List;

import br.com.agroweb.api.domain.dto.ServicoDto;
import br.com.agroweb.api.domain.dto.ServicoPropostaDto;

public interface ServicoPropostaService {
	
	ServicoPropostaDto create(ServicoPropostaDto servicoPropostaDto);
	
	List<ServicoDto> getAllServico();
	
	List<ServicoPropostaDto> findByProposta(Long id);
}
