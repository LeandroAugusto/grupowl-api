package br.com.agroweb.api.service;

import java.util.List;

import br.com.agroweb.api.domain.dto.PessoaDto;
import br.com.agroweb.api.domain.dto.PropriedadeDto;
import br.com.agroweb.api.domain.dto.VinculoDto;

public interface PropriedadeService {

	PropriedadeDto getById(Long id);
	
	PropriedadeDto getByCpfCnpj(String cpfCnpj);

	List<PropriedadeDto> getByFilter(String nomeRazaoSocial, String cpfCnpj);

	PropriedadeDto create(PropriedadeDto propriedadeDto);
	
	VinculoDto createVinculo(VinculoDto vinculoDto);

	PropriedadeDto update(Long id, PropriedadeDto propriedadeDto);

	void delete(Long id);
	
	List<PropriedadeDto> findByPessoa(String cpfCnpj);
	
	List<PessoaDto> getbyPropriedadeId(Long id);
}
