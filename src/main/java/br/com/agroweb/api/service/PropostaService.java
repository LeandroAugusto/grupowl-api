package br.com.agroweb.api.service;

import java.util.List;

import br.com.agroweb.api.domain.dto.PropostaDto;

public interface PropostaService {
	
	PropostaDto create(PropostaDto propostaDto);
	
	List<PropostaDto> findByFilters(String cpfCnpj);
	
	PropostaDto findById(Long id);
}
