package br.com.agroweb.api.service;

import java.util.List;

import br.com.agroweb.api.domain.Estado;

public interface EstadoService {
	
	Estado getBySigla(String sigla);
	
	List<Estado> getAll();
}
