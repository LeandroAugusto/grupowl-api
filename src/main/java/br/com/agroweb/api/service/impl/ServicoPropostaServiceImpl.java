package br.com.agroweb.api.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.agroweb.api.domain.Servicos;
import br.com.agroweb.api.domain.ServicosProposta;
import br.com.agroweb.api.domain.dto.ServicoDto;
import br.com.agroweb.api.domain.dto.ServicoPropostaDto;
import br.com.agroweb.api.domain.mapper.AgroPortalMapper;
import br.com.agroweb.api.repository.ServicoPropostaRepository;
import br.com.agroweb.api.repository.ServicoRepository;
import br.com.agroweb.api.service.ServicoPropostaService;

@Service
public class ServicoPropostaServiceImpl implements ServicoPropostaService{

	@Autowired
	private AgroPortalMapper mapper;

	private ServicoPropostaRepository servicoPropostaRepo;
	
	private ServicoRepository servicoRepo;

	@Autowired
	public ServicoPropostaServiceImpl(ServicoPropostaRepository servicoPropostaRepo, ServicoRepository servicoRepo) {
		this.servicoPropostaRepo = servicoPropostaRepo;
		this.servicoRepo = servicoRepo;
	}
	
	@Override
	public ServicoPropostaDto create(ServicoPropostaDto servicoPropostaDto) {
		ServicosProposta servicoProposta = mapper.servicoPropostaToModel(servicoPropostaDto);
		return mapper.servicoPropostaToDto(servicoPropostaRepo.save(servicoProposta));
	}

	@Override
	public List<ServicoDto> getAllServico() {
		List<Servicos> list = servicoRepo.findAll();
		return list.stream().map(item -> mapper.servicoToDto(item)).collect(Collectors.toList());
	}

	@Override
	public List<ServicoPropostaDto> findByProposta(Long id) {
		return servicoPropostaRepo.findByPropostaId(id)
				.stream().map(item -> mapper.servicoPropostaToDto(item))
				.collect(Collectors.toList() );
	}
}