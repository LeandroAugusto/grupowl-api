package br.com.agroweb.api.service.impl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.agroweb.api.domain.Pessoa;
import br.com.agroweb.api.domain.dto.PessoaDto;
import br.com.agroweb.api.domain.enums.TipoPessoaEnum;
import br.com.agroweb.api.domain.mapper.AgroPortalMapper;
import br.com.agroweb.api.repository.PessoaRepository;
import br.com.agroweb.api.service.PessoaService;

@Service
public class PessoaServiceImpl implements PessoaService {

	@Autowired
	private AgroPortalMapper mapper;

	private PessoaRepository pessoaRepo;

	@Autowired
	public PessoaServiceImpl(PessoaRepository pessoaRepo) {
		this.pessoaRepo = pessoaRepo;
	}

	@Override
	public PessoaDto getById(Long id) {
		Optional<Pessoa> pessoa = pessoaRepo.findById(id);
		return mapper.pessoaToDto(pessoa.orElse(null));
	}

	@Override
	public List<PessoaDto> getByFilter(String nomeRazaoSocial, String cpfCnpj, TipoPessoaEnum tipoPessoa) {
		List<Pessoa> listPessoa = pessoaRepo.findByFilters();
		if(nomeRazaoSocial != null) {
			listPessoa = listPessoa.stream()
					.filter(item -> nomeRazaoSocial.equalsIgnoreCase(item.getNomeRazaoSocial()))
					.collect(Collectors.toList());
		}
		if(cpfCnpj != null) {
			listPessoa = listPessoa.stream()
					.filter(item -> item.getCpfCnpj().equals(cpfCnpj))
					.collect(Collectors.toList());
		}
		if(tipoPessoa != null) {
			listPessoa = listPessoa.stream()
					.filter(item -> item.getTipoPessoa().equals(tipoPessoa))
					.collect(Collectors.toList());
		}
		return listPessoa.stream().map(pessoa -> mapper.pessoaToDto(pessoa)).collect(Collectors.toList());
	}

	@Override
	public PessoaDto create(PessoaDto pessoaRequest) {
		Pessoa pessoa = mapper.pessoaToModel(pessoaRequest);
		return mapper.pessoaToDto(pessoaRepo.save(pessoa));
	}

	@Override
	public PessoaDto update(Long id, PessoaDto pessoaRequest) {
		PessoaDto pessoa = getById(id);
		BeanUtils.copyProperties(pessoaRequest, pessoa, "id");
		return mapper.pessoaToDto(pessoaRepo.save(mapper.pessoaToModel(pessoa)));
	}

	@Override
	public void delete(Long id) {
		pessoaRepo.deleteById(id);
	}

	@Override
	public PessoaDto getByCpfCnpj(String cpfCnpj) {
		Optional<Pessoa> pessoa = pessoaRepo.findByCpfCnpj(cpfCnpj);
		return mapper.pessoaToDto(pessoa.orElse(null));
	}
}